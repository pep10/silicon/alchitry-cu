`include "../ip/pep10si/hdl/component/counter.v"
`include "../ip/pep10si/hdl/component/update_reg.v"
`include "../ip/pep10si/hdl/component/mux4xN.v"
`include "../ip/pep10si/hdl/component/seg7decoder.v"
`include "../ip/pep10si/hdl/component/demux2x4.v"
module seg7ctrl (
	input wire clock,
	input wire [3:0] update_0,
	input wire [3:0] update_1,
	input wire [3:0] update_2,
	input wire [3:0] update_3,
	input wire [3:0] en_update,
	output [3:0] io_seg_sel,
    output [6:0] io_seg_dat,
    output       io_seg_dp,
	output [10:0] counter_out);

	//wire [18:0] counter_out;
	counter #(.WIDTH(11)) counter(clock, 1'b0, counter_out);

	wire [3:0] en_primary;
	demux2x4 demux(1'b1, counter_out[10:9], en_primary);

	wire difference[3:0];
	wire [3:0] primary[3:0];
	//wire enable;
	//or(enable, difference[0], difference[1], difference[2], difference[3]);
	
	update_reg #(.WIDTH(4)) v0(.clock(clock), .en_update(en_update[0]), .en_primary(en_primary[0]),
		.update(update_0), .difference(difference[0]), .primary(primary[0]));
	update_reg #(.WIDTH(4)) v1(.clock(clock), .en_update(en_update[1]), .en_primary(en_primary[1]),
		.update(update_1), .difference(difference[1]), .primary(primary[1]));
	update_reg #(.WIDTH(4)) v2(.clock(clock), .en_update(en_update[2]), .en_primary(en_primary[2]),
		.update(update_2), .difference(difference[2]), .primary(primary[2]));
	update_reg #(.WIDTH(4)) v3(.clock(clock), .en_update(en_update[3]), .en_primary(en_primary[3]),
		.update(update_3), .difference(difference[3]), .primary(primary[3]));

	// Todo: Add enable line to multiplexer
	wire [3:0] mux_out;
	mux4xN #(.WIDTH(4)) input_mux(primary[0], primary[1], primary[2], primary[3], counter_out[10:9], mux_out);
	wire [6:0] decoder_out;
	seg7decoder decoder(mux_out, decoder_out);

	always@(posedge clock) begin
		io_seg_sel = ~en_primary;
		io_seg_dat = decoder_out;
		io_seg_dp = 1 ;
	end



endmodule