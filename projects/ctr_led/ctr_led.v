module top(output wire[7:0] builtin_led, 
    output [7:0] io_led0,
    output [7:0] io_led1,
    output [7:0] io_led2,
    output [3:0] io_seg_sel,
    output [6:0] io_seg_dat,
    output       io_seg_dp,
    input  [7:0] io_switch0,
    input  [7:0] io_switch1,
    input  [7:0] io_switch2,
    input clock, input reset);
    // Never write directly to IO pins, always buffer/
    reg [31:0] ctr;
    initial ctr = 0;

    always @(posedge clock) begin
        if(reset == 0) ctr <= 0;
        else ctr <= ctr + 1;

    end

    buf(builtin_led, ctr[31:24]);
    buf(io_led0, io_switch0);
    buf(io_led1, io_switch1);
    buf(io_led2, io_switch2);
    buf(io_seg_sel, 4'b0000);
    buf(io_seg_dat, 7'b0000000);
    buf(io_seg_dp, 1'b0);
endmodule

