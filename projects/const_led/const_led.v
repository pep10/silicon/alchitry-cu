module top(output wire[7:0] builtin_led);
    // Never write directly to IO pins, always buffer/
    buf(builtin_led, 8'hAD);
endmodule

