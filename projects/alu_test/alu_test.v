`include "../../parts/seg7ctrl.v"
`include "../../ip/pep10si/hdl/subsystem/alu.v"
module top(output wire[7:0] builtin_led, 
    output [7:0] io_led0,
    output [7:0] io_led1,
    output [7:0] io_led2,
    output [3:0] io_seg_sel,
    output [6:0] io_seg_dat,
    output       io_seg_dp,
    input  [7:0] io_switch0,
    input  [7:0] io_switch1,
    input  [7:0] io_switch2,
    input clock, input reset
);
    
    wire [3:0] status_temp;
    wire [7:0] out_temp;
    alu alu(.A(io_switch2), .B(io_switch1), .select(io_switch0[5:1]), .carry_in(io_switch0[0]),
        .out(out_temp), .status_n(status_temp[3]), .status_z(status_temp[2]),
        .status_v(status_temp[1]), .status_c(status_temp[0]));

    // Never write directly to IO pins, always buffer.
    buf(builtin_led, status_temp);
    buf(io_led2, io_switch2);
    buf(io_led1, io_switch1);
    buf(io_led0, out_temp);

    wire [7:0] temp;
    wire [3:0] enable = 4'hf;
    seg7ctrl ctrl(clock,4'h0, 4'h0, out_temp[7:4], out_temp[3:0], enable, io_seg_sel, io_seg_dat, io_seg_dp, temp);

endmodule

