`include "../../parts/seg7ctrl.v"
module top(output wire[7:0] builtin_led, 
    output [7:0] io_led0,
    output [7:0] io_led1,
    output [7:0] io_led2,
    output [3:0] io_seg_sel,
    output [6:0] io_seg_dat,
    output       io_seg_dp,
    input  [7:0] io_switch0,
    input  [7:0] io_switch1,
    input  [7:0] io_switch2,
    input clock, input reset);
    // Never write directly to IO pins, always buffer/
    reg [39:0] ctr;
    initial ctr = 0;

    always @(posedge clock) begin
        if(reset == 0) ctr <= 0;
        else ctr <= ctr + 1;

    end

    buf(builtin_led, ctr[31:24]);
    buf(io_led0, io_seg_sel);
    buf(io_led1, io_seg_dat);
    //buf(io_led2, io_switch2);
    wire [7:0] temp;
    buf(io_led2, temp);
    wire [3:0] enable = 4'hf;
    seg7ctrl ctrl(clock, ctr[39:36], ctr[35:32], ctr[31:28], ctr[27:24], enable, io_seg_sel, io_seg_dat, io_seg_dp, temp);

endmodule

