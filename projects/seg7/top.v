`include "../../parts/seg7ctrl.v"
module top();
    wire [3:0] io_seg_sel;
    wire [6:0] io_seg_dat;
    wire       io_seg_dp;
    reg [16:0] ctr;
    initial ctr = 0;

    always begin
		#2;
        ctr <= ctr + 1;
    end

    wire [3:0] enable = 4'hf;
    seg7ctrl seg7(ctr[0] == 1'b0, ctr[15:12], ctr[11:8], ctr[7:4], ctr[3:0], enable, io_seg_sel, io_seg_dat, io_seg_dp);
	initial begin
		$monitor("%h\t%b %h %h%h%h%h %h%h%h%h", ctr[16:0], io_seg_sel, seg7.mux_out,
			seg7.primary[0], seg7.primary[1], seg7.primary[2], seg7.primary[3],
			seg7.update_0, seg7.update_1, seg7.update_2, seg7.update_3);
	end
	initial begin
		#2048;
		$finish;
	end
endmodule

